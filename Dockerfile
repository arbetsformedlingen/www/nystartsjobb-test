FROM nginx:1.27.4-alpine3.21-slim

COPY infrastructure/default.conf /etc/nginx/conf.d/
COPY infrastructure/nginx.conf /etc/nginx/

COPY src/ /usr/share/nginx/html/

# HACK to create a published build time
RUN echo '<h4>Releasedatum</h4>' > /usr/share/nginx/html/build.html &&\
    date >> /usr/share/nginx/html/build.html  &&\
    echo '<h4>Version</h4>' >> /usr/share/nginx/html/build.html &&\
    echo '<img src="https://img.shields.io/badge/dynamic/json?color=blue&label=Commit%20hash&query=%24%5B0%5D.short_id&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F33528686%2Frepository%2Fcommits%3Fref_name%3Donprem"/>' >> /usr/share/nginx/html/build.html &&\
#RUN date > /usr/share/nginx/html/build.txt
    ln -sf /dev/stdout    /var/log/nginx/nginx-access.log &&\
    ln -sf /dev/stderr    /var/log/nginx/nginx-error.log

EXPOSE 8080
STOPSIGNAL SIGQUIT


CMD ["nginx", "-g", "daemon off; error_log stderr info;"]
