[![Arbetsförmedlingens logo](https://arbetsformedlingen.se/webdav/files/logo/logo.svg)](https://arbetsformedlingen.se)

Till e-tjänsten [självtest nystartsjobb](https://arbetsformedlingen.se/nystartsjobb)

<small>Finns också en [testmiljö](https://arbetsformedlingen.gitlab.io/www/nystartsjobb-test/)</small>

---

# Självtest nystartsjobb

Denna plats hanterar källkoden för självtestet. [Självtestet](https://nystartsjobb-sjalvtest.arbetsformedlingen.se/) är ett enkelt sätt för användaren att utröna om villkoren för [nystartsjobb](https://arbetsformedlingen.se/kontakt/for-arbetssokande/vanliga-fragor-och-svar#humany-asok-faq=/g961-vad-aer-nystartsjobb-och-hur-ansoeker-jag;take:20) är uppfyllda genom att svara på några frågor. Villkoren för nystartsjobb definieras i [förordningen för nystartsjobb](https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/forordning-201843-om-stod-for-nystartsjobb_sfs-2018-43).

> Det är i form av källkod som programmerare skriver, rättar och förändrar datorprogram
> -- <cite>[Wikipedia](https://sv.wikipedia.org/wiki/K%C3%A4llkod)</cite>
>
> När källkoden också är öppen kan alla granska eller ge förbättringsförslag.

## Version

[![Bild på versionsnummer](https://img.shields.io/badge/dynamic/json?color=green&label=Version%20%28commit%20hash%29&query=%24%5B0%5D.short_id&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F33528686%2Frepository%2Fcommits%3Fref_name%3Donprem)](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/commits/onprem)

Version 0.9.2

[Changeloggen](Changelog.md) beskriver vad respektive release innehåller.

Versionen uppdateras manuellt av teamet vid förändringar, koden (commit hash) anger exakt vilken källkod som körs just nu i produktion.

1. [Ladda ner källkoden som körs i produktion just nu](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/archive/onprem/nystartsjobb-test-onprem.zip).
2. [Eller, bläddra bland källkoden som körs i produktion](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/tree/onprem)

> Om du vill bokmärka vilken version som körs för tillfället så använd den exakta versionen.

## Målgrupp

Primärt för de som utvecklar testet.

Men alla får gärna läsa och undersöka källkoden och ge förbättringsförslag.

**Denna sida inte är en supportkanal.**

#### Har du en fråga om nystartsjobb?

Prova att besöka:

1. [Vanliga frågor och svar om nystartsjobb](https://arbetsformedlingen.se/kontakt/for-arbetssokande/vanliga-fragor-och-svar#humany-asok-faq=/g961-vad-aer-nystartsjobb-och-hur-ansoeker-jag;take:20)
2. [Startsida för nystartsjobb](https://arbetsformedlingen.se/for-arbetssokande/extra-stod/stod-a-o/nystartsjobb)
3. [Vid tekniska problem](https://arbetsformedlingen.se/kontakt/tekniska-problem)

## Få insyn i hur testet är byggt

Det finns fördelar med att tekniken bakom e-tjänster är öppen:

1. Du kanske vill granska om lösningen är bra?
2. Du kanske vill veta om vilka frågor som ställs eller inte ställs?
3. Testet utlovar att ingen information lagras om individen. Här kan du kontrollera att det stämmer.
4. Du är så trött på dålig teknik. [Beskriv vad som inte fungerar](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/issues).

## Arkitekturen

Hela självtestet laddas i användarens webbläsare [via index.html](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/blob/master/public/index.html). På det sättet registrerar inte myndigheten någon information om vad användaren svarar i testet. Teoretiskt kan myndigheten se vilken dator (ip-nummer) som har laddat ner självtestet ([hur hanterar arbetsförmedlingen personuppgifter](https://arbetsformedlingen.se/om-webbplatsen/juridisk-information/sa-hanterar-vi-dina-personuppgifter)), men inte om individen har gjort testet eller inte.

| Rad  | Läsanvisning index.html |
| --- | --- |
| [Rad 150](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/blob/master/src/index.html#L150) | Definition om vilka frågor som ingår |
| [Rad 275](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/blob/master/src/index.html#L275) | Formuleringar av villkoren om de uppfylls |
| [Rad 512](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/blob/master/src/index.html#L512) | Regelmotor (traversering av frågor beroende på vad man svarar) |

#### Återanvändning

Filen index.html kan exempelvis kommuner eller fristående aktörer använda för att klippa in testet på sina hemsidor. Alla har rätt att använda index.html i det syftet.

## Driftmiljö

Testet består av [katalogen public](https://gitlab.com/arbetsformedlingen/www/nystartsjobb-test/-/tree/master/public) som flyttas till webbservern vid produktionstillfället. Dvs en statisk webbapplikation.

Webbservern driftas av Arbetsförmedlingen. Alla förändringar i detta repository deployas/sjösätts automatiskt baserat på ett [egenutvecklat verktyg](https://gitlab.com/arbetsformedlingen/devops/aardvark) till test eller produktion. Driftmiljön bygga upp med kubernetes/openshift.

## Licens

Apache 2.0. Se licensfilen. Kortfattat kan den som vill läsa, modifiera och vidaredistribuera källkoden.

Arbetsförmedlingens logotyp får endast användas av Arbetsförmedlingen och där Arbetsförmedlingen är avsändare, [mer info](https://arbetsformedlingen.se/om-oss/press/logotyp).

## Använder
1. Designsystem - https://designsystem.arbetsformedlingen.se/
2. CI/CD - https://gitlab.com/arbetsformedlingen/devops/aardvark
3. Webbserver - https://www.nginx.com/

## Av

Arbetsförmedlingen är avsändare för plattformen https://jobtechdev.se. Syftet med plattformen är att samarbeta baserat på öppna data och ny teknik, för en bättre arbetsmarknad.
