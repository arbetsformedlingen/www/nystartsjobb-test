# Changelog
Väsentliga förändringar kommer dokumenteras i denna fil.

Formatet enligt [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
och versionshanteringen följer [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.9.3] - 2023-12-15
### Changed
- Ny tolkning att arbetsgivaren minst kan få ett stöd avseende EN arbetsgivargift istället för TVÅ.
- Tagit bort kravet i fråga fyra att strafftiden vid en fängelsevistelse måste vara minst 12 månader. 

## [0.9.2] - 2022-11-08
### Changed
- Bakgrundsfärg resultatsida #f5f5f5
- Privacy policy. Arbetsförmedlingen har uppdaterat sin privacy policy med beskrivning om hur ip-nummer hanteras.

### Added
- Spårning. Lagt till spårning om testet startas (anonymt för användaren).

### Fixed
- Bytt namn på widget.html till index.html.

## [0.9.1] - 2022-10-15
### Changed
- BUG fråga 7, knapparna (ja/nej) placerades över frågan
- Bakgrundsfärg resultatsida #e6e6ef som är en färgton av #05035a, från [tint and shade generator](https://maketintsandshades.com/#05035A)
- BUG vitt streck under rubrik, satt färgen genom att överlagra --digi--ui--color--primary

### Added
- Timer. Rensar alla val efter 10 minuter, primärt för de som glömmer webbläsarfönstret öppet.
- Build.html. Konvention att build.html ger lite info om bygget som körs för tillfället.

### Fixed
- Rensat. Tagit bort onödiga kataloger och filer. (Exempelvis .DS_Store etc)
- Flyttat. Config-filer för nginx ligger i infrastructure

## [0.9.0] - 2022-10-12
### Added
- Ändrat färgval baserat på arbetsförmedlingens designers
- Lagt till länk till källkoden

### Fixed
- Skrivit en Readme

### Removed
- Tagit bort länk till undersökningen
- Tagit bort knapparna "väg vidare" på resultatsidan
